/**
 * Created by well on 06.12.16.
 */

var arr = [
    {
        "id":"132465798",
        "OwnedBy":"54ed8cf266a14edb74dc5d15",
        "rooms":[
            {
                "accessories":[
                    {
                        "services":[

                        ],
                        "id":"4",
                        "accessoryName":"ACSDSD"
                    },
                    {
                        "services":[
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":1
                                    }
                                ],
                                "id":"s-1",
                                "serviceName":"Lamp"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":1
                                    }
                                ],
                                "id":"s-2",
                                "serviceName":"Lock"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"0.005"
                                    }
                                ],
                                "id":"s-3",
                                "serviceName":"Ligth Sensor"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"1"
                                    }
                                ],
                                "id":"s-4",
                                "serviceName":"Motion Sensor"
                            }
                        ],
                        "id":"3",
                        "accessoryName":"Net ACS"
                    }
                ],
                "id":"1",
                "roomName":"My Room Name"
            },
            {
                "accessories":[
                    {
                        "services":[
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"s-1",
                                "serviceName":"Lamp"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"1"
                                    }
                                ],
                                "id":"s-2",
                                "serviceName":"Lock"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"0.005"
                                    }
                                ],
                                "id":"s-3",
                                "serviceName":"Ligth Sensor"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"1"
                                    }
                                ],
                                "id":"s-4",
                                "serviceName":"Motion Sensor"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"0"
                                    }
                                ],
                                "id":"s-4",
                                "serviceName":"Motion Firt Sensor"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"ch-1",
                                        "characteristicValue":"1"
                                    }
                                ],
                                "id":"s-1",
                                "serviceName":"Contact Sensor"
                            }
                        ],
                        "id":"3",
                        "accessoryName":"Large Accessory"
                    }
                ],
                "id":"2",
                "roomName":"Room 2"
            }
        ],
        "updateBy":"54ed8cf266a14edb74dc5d15",
        "Createdate":"2016-12-01T07:38:16.720Z",
        "last_update":"2016-12-01T14:39:30.022Z",
        "homeName":"MY HOME"
    },
    {
        "id":"4BF44521-B2E8-5168-8447-6F7AB9230DD9",
        "OwnedBy":"54ed8cf266a14edb74dc5d15",
        "rooms":[
            {
                "accessories":[
                    {
                        "services":[
                            {
                                "characteristics":[
                                    {
                                        "id":"9876BD6E-AC16-5653-9F9D-590B0E4E17B5",
                                        "characteristicValue":24
                                    },
                                    {
                                        "id":"AD1C6E49-9EF0-587E-930A-9A29B24CBE60",
                                        "characteristicValue":"Model Name updated2"
                                    },
                                    {
                                        "id":"66302923-16AC-53E9-8768-706252FC5570",
                                        "characteristicValue":"Model Name"
                                    },
                                    {
                                        "id":"7F15E7D4-0FDF-59DA-84B2-2AEECE7B758A",
                                        "characteristicValue":"Dining Room"
                                    },
                                    {
                                        "id":"772AEB6D-1FF2-5307-A0B2-ECADE34B1884",
                                        "characteristicValue":"13GGGX30HK76T"
                                    }
                                ],
                                "id":"71C34AC5-4BE7-5CB0-95B7-C04B0E11C684",
                                "serviceName":"Dining Room"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"7D55795C-2E11-52B4-9013-088177A71EE8",
                                        "characteristicValue":"Lock Management 08"
                                    },
                                    {
                                        "id":"E7D2365B-CEB6-57C2-8FA0-426FEB418BF6",
                                        "characteristicValue":"Turn ON"
                                    },
                                    {
                                        "id":"97BA5B70-1F32-566B-AB5E-F17DC68F1E23",
                                        "characteristicValue":" value"
                                    },
                                    {
                                        "id":"FDF8F073-0961-5B30-9B3F-7E4007B193C5",
                                        "characteristicValue":"<000100>"
                                    },
                                    {
                                        "id":"C190BE53-87A6-5442-BBBE-E35285CA192A",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"9E590EB0-CFF9-5DB8-9657-BE3855FAB7C8",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"E56B52F9-3BE8-5EC1-A59E-E48BA3444977",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"9C7E8FF3-3BFA-5E45-9573-D8D84EEFE3CF",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"DC5ACC93-1C84-56B1-921D-D8D3DDD77CCC",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"57A60675-CE84-5393-85C5-83EE4D9568CD",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"ECE7239D-FA6C-552F-BFB1-23E5D324CE83",
                                "serviceName":"Lock Management 08"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"D6E44581-9B82-5168-9451-E6A7833D5813",
                                        "characteristicValue":"Lock Mechanism D9"
                                    },
                                    {
                                        "id":"13F20C6F-5EBB-563B-9118-180D18D1791E",
                                        "characteristicValue":1
                                    },
                                    {
                                        "id":"DE4BC982-F246-5DE0-AE01-9931E95E91DC",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"4A435D10-11F5-5E06-A8BC-516663233540",
                                "serviceName":"Lock Mechanism D9"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"DF78BC48-E4E2-5166-8C37-70ED72B0378B",
                                        "characteristicValue":"Contact Sensor ED"
                                    },
                                    {
                                        "id":"9933DB4B-9EAF-57ED-B964-E7A26C1E7402",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"1CE4E799-467B-5D80-B4BE-E771B1DB048B",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"B93EC043-42B2-5D7D-A8CD-43F9AA2049C8",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"7E1ED939-87FB-52E5-ACB3-16E21614ABB3",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"B2E71957-1D5D-5FE8-B6E3-F281B1D6789A",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"0A2EDFBE-D5EC-5869-858B-72BFB03617FD",
                                "serviceName":"Contact Sensor ED"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"AD926721-41C5-5F9E-8195-190B8E662755",
                                        "characteristicValue":"Motion Sensor 08"
                                    },
                                    {
                                        "id":"25F68286-63B1-5D19-A1B1-108E0A0E38F2",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"6FC0006C-0D3C-548F-9275-A08503AE415A",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"D112A499-803F-550D-9974-4DD75ACDC81A",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"C143C0EB-FC6B-533D-BA35-1398B20CEEA5",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"A46A6AFD-15D6-54A9-8FD4-42007A22CD59",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"E739173F-7B94-5BCD-97CA-2A24CBBAAAA1",
                                "serviceName":"Motion Sensor 08"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"781C008C-E889-51B7-9212-FB6FE3B0BE0B",
                                        "characteristicValue":"Lightbulb 19"
                                    },
                                    {
                                        "id":"71AF9700-67D0-57E0-9C2C-38AD80A34264",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"07462A64-A367-5B64-8273-ACB116E8D65E",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"1DE62CD9-DA8E-5D46-92D5-19C21B259ED6",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"180EDEA4-DA2B-5F9E-B040-78016B419184",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"56F20DB5-A4D4-54A5-A051-CF52E21CA206",
                                "serviceName":"Lightbulb 19"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"7E811211-9123-5FEC-AA14-32FEB73D080F",
                                        "characteristicValue":"Light Sensor F8"
                                    },
                                    {
                                        "id":"6AFF8F41-80E0-5671-9875-08E6AE450A97",
                                        "characteristicValue":0.0001
                                    },
                                    {
                                        "id":"FD125317-10CA-5F45-AE65-160D85CC9F89",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"0F69DAEE-91A3-5C64-9BB7-5696B0930269",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"51E21695-9141-54A2-8FAE-752579CB7898",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"71E8ABEE-F43C-571E-BAF1-5D9946DC5D2E",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"67AFDE72-E9C7-5053-9F40-47B01D5C49A4",
                                "serviceName":"Light Sensor F8"
                            },
                            {
                                "characteristics":[
                                    {
                                        "id":"48315604-8583-521D-BC1B-E91AB113513F",
                                        "characteristicValue":"Outlet 81"
                                    },
                                    {
                                        "id":"55771C8F-2A13-54E9-91FC-6C809AD39664",
                                        "characteristicValue":0
                                    },
                                    {
                                        "id":"E2243223-9DC4-512D-84C8-1ECB26FB639A",
                                        "characteristicValue":0
                                    }
                                ],
                                "id":"DC0ACCBB-516B-5FC4-965F-1349146567B2",
                                "serviceName":"Outlet 81"
                            }
                        ],
                        "id":"55133F1E-42F3-5270-962F-37D89E8B4C18",
                        "accessoryName":"Dining Room"
                    },
                    {
                        "services":[

                        ],
                        "id":"55133F1E-42F3-5270-962F-1111111111111",
                        "accessoryName":"TV"
                    },
                    {
                        "services":[

                        ],
                        "id":"55133F1E-42F3-5270-962F-1111111111112",
                        "accessoryName":"MUSIC"
                    },
                    {
                        "services":[

                        ],
                        "id":"55133F1E-42F3-5270-962F-1111111111113",
                        "accessoryName":"MUSIC BIG"
                    }
                ],
                "id":"07252F3A-273F-5C21-BDD4-B6BD08CB4432",
                "roomName":"Room 1"
            }
        ],
        "updateBy":"54ed8cf266a14edb74dc5d15",
        "Createdate":"2016-11-24T09:37:33.850Z",
        "last_update":"2016-11-25T15:25:59.062Z",
        "homeName":"Home3517253376"
    }
];

function getValue(el) {
    if (typeof el === 'string') {
        el = document.querySelector(el)
    }
    if (!el) {
        return null;
    }
    return el.value || '';
}

function getEl(query) {
    if (typeof query === 'string') {
        query = document.querySelector(query)
    }
    if (!query) {
        return null;
    }
    return query
}


var id = getValue('#inputid');
var name;

var el = getEl('#inputname');
el.value = name;

function newname(id, name) {
    for (var i = 0; i < arr.length; i++) {

        if (arr[i].id == id) {
            arr[i].homeName = name;
            alert(arr[i].homeName)
        }




    }
    ;


}




// in  (id,NEW_homeName)
// out true/false





